this.name        = "star-fuel"; 
this.author      = "Redspear"; 
this.copyright   = "2018 Redspear";
this.licence     = "CC BY-NC-SA 4.0"; 
this.description = "Script to boost injectors with fuel scooped rather than bought"; 
this.version     = "1.1";

"use strict";

this.$radius=6000;
this.libSettings = {Name:this.name, Display:"Config", Alias:"Star Fuel", Alive:"libSettings",
SInt:{
	S0:{Name:"$radius", Def:6000, Min:0, Max:9999, Desc:"Min Radius", Float:true},
	Info:"0 - Minimum planet radius for star fuel"},
};

//	set injector bonus and test for last source of fuel
this.startUpComplete = this.playerBoughtNewShip = function()
{
	this._SFfalse = player.ship.injectorSpeedFactor;
	this._SFtrue = player.ship.injectorSpeedFactor *2;

	
	if (player.ship.equipmentStatus("EQ_STAR_FUEL") == "EQUIPMENT_OK") {
		player.ship.injectorSpeedFactor = _SFtrue;
		}
		else {
		player.ship.injectorSpeedFactor = _SFfalse;
		}

	if (missionVariables.Starfuel_radius) this.$radius = parseFloat(missionVariables.Starfuel_radius);
	if (worldScripts.Lib_Config) worldScripts.Lib_Config._registerSet(this.libSettings);
}

//	check if scooped fuel grants bonus
this.shipScoopedFuel = function()
{
	if (system.info.radius > this.$radius) {	//	based on planet rather than sun radius (former visible on galactic chart)
		player.ship.awardEquipment("EQ_STAR_FUEL");
		player.ship.injectorSpeedFactor = _SFtrue;
		player.consoleMessage ("Now Scooping PREMIUM Fuel");
		}
		else{
		player.ship.removeEquipment("EQ_STAR_FUEL");
		player.ship.injectorSpeedFactor = _SFfalse;
		}
}

//	remove bonus and equipment marker for saved games
this.playerBoughtEquipment = function(equipment)
{
    if (equipment === "EQ_FUEL"){
		player.ship.injectorSpeedFactor = _SFfalse;
		player.ship.removeEquipment("EQ_STAR_FUEL");
		}
}


//	ensure no 'save stacking' of bonus
this.playerWillSaveGame = function()
{
     player.ship.injectorSpeedFactor = _SFfalse;
     missionVariables.Starfuel_radius = this.$radius;
}

